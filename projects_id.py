# projects_id.py
#
# DESCRIPTION: Returns JSON with all projects summarized info: id, name and
#   parent information.
# EXECUTION: python3 projects_id.py
# CREDENTIALS: .env (See env.example)

import requests
import json
import re
from dotenv import load_dotenv
from os import environ
# from math import ceil
from urllib import response

load_dotenv()

DOMAIN: str = environ.get('DOMAIN')
LINES_IN_PAGE: int = int(environ.get('PAGE'))
KEY: str = environ.get('KEY')
DEBUG: bool = int(environ.get('DEBUG'))
r: response
headers: dict
project: dict
result: list
page: int
offset: int = 0


def get_projects_summarized(headers: dict, url: str, ) -> str:
    idx: int

    if DEBUG:
        print(f"{url=}")
    r = requests.get(url, headers=headers,)

    # print(r.json())

    if DEBUG:
        print(f"Projectes totals {r.json()['total_count']}")

    result = []
    rounds = round(int(r.json()['total_count']) / LINES_IN_PAGE)

    if DEBUG:
        print(f"rounds = {rounds}")

    for page in range(0, rounds + 1):
        if DEBUG:
            print(len(result))
        idx = 1
        for projecte in r.json()['projects']:
            projecte_summarized = {'id': projecte.get('id'),
                                   'name': projecte.get('name'),
                                   'parent': projecte.get('parent',
                                                          '(1st level project')
                                   }
            result.append(projecte_summarized)
            if DEBUG:
                print(f"{idx} {projecte_summarized=}")
            idx += 1
        offset = LINES_IN_PAGE * (page + 1)
        if DEBUG:
            print(f"{offset=}")
        url = re.sub('offset=([0-9]+)', 'offset={}'.format(offset), url)
        if DEBUG:
            print(f"{url=}")
        r = requests.get(url, headers=headers,)

    if DEBUG:
        print(f"{len(result)=}")

    return json.dumps(result)


if __name__ == "__main__":
    headers = {'Content-Type': 'application/json',
               'X-Redmine-API-Key': KEY}
    url = "{domain}/projects.json?offset={offset}&limit={limit}"\
        .format(domain=DOMAIN, offset=offset, limit=LINES_IN_PAGE,)

    print(get_projects_summarized(headers, url))
