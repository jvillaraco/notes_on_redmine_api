# change_project_for_issue.py
#
# DESCRIPTION: Update project for listes issues.
# EXEC:
#   python3 change_project_for_issue.py < change_project_for_issue_input.txt
# FORMAT FOR INPUT FILE:
#   1st line: <project_id>
#   Next lines: <issues_to change_its_project>
# CREDENTIALS: (token) file: .env

from ast import arguments
import sys
import requests
import json
from dotenv import load_dotenv
from urllib import response
from os import environ
from projects_id import get_projects_summarized

load_dotenv()

DOMAIN: str = environ.get('DOMAIN')
LINES_IN_PAGE: int = int(environ.get('PAGE'))
KEY: str = environ.get('KEY')
DEBUG: bool = int(environ.get('DEBUG'))
r: response
headers: dict
project: dict
result: list
page: int
offset: int = 0

projs_summ: dict
proj_id: int
project_id_and_issues_to_change: dict


def change_project_for_issue(project_id_and_issues_to_change: dict,
                             headers: dict) -> None:
    issue_id: int

    for issue_id in project_id_and_issues_to_change['issues']:
        url = "{domain}/issues/{issue_id}.json".format(domain=DOMAIN,
                                                       issue_id=issue_id)
        print(url)
        r = requests.get(url, headers=headers)
        # print(r.text)
        print(f"Issue previous to change: {r.json()}")

        data = {"issue":
                {"project_id":
                    project_id_and_issues_to_change['project_id']}
                }

        print(f"data: {data}")

        r = requests.put(url, headers=headers, json=data)
        print(r)

        r = requests.get(url, headers=headers)
        # print(r.text)
        print(f"Issue after change: {r.json()}\n")


def get_input() -> dict:
    first_line: bool = True
    line: str
    arguments: dict = {}
    issues_to_change: list = []

    for line in sys.stdin:
        if first_line:
            arguments['project_id'] = line.strip()
            first_line = False
        else:
            issues_to_change.append(line.strip())
    arguments['issues'] = issues_to_change

    print(arguments)
    print()
    return arguments


if __name__ == "__main__":
    headers = {'Content-Type': 'application/json',
               'X-Redmine-API-Key': KEY}
    # url = "{domain}/issues/{issue_id}.json".format(domain=DOMAIN)

    project_id_and_issues_to_change = get_input()

    change_project_for_issue(project_id_and_issues_to_change, headers)
